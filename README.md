###Procedimentos para execu��o do projeto
Pr�-requisitos
 - Java 8 (http://www.oracle.com/technetwork/pt/java/javase/downloads/index.html)
 - Playframework 2.5.12 com Activator (https://www.playframework.com/download)
 - NodeJS 6.10.0 (https://nodejs.org/en/download/current/)
 - PostgreSQL 9.6 (https://www.postgresql.org/download/
 

###Backend

1) Instalar o banco de dados Postgres, ap�s instalado criar banco de dados com nome 'pixeon' 

2) Configurar os par�metros de conex�o com o banco de dados no arquivo conf/application.conf
db.default.driver=org.postgresql.Driver
db.default.url="jdbc:postgresql://localhost:5432/pixeon"
db.default.user=postgres
db.default.password="postgres"

3) Instalar o PlayFramework com Activator.

4) Para rodar o backend execute: activator run (Todas as depend�ncias do compilador Scala SBT ser�o baixadas e resolvidas nesta etapa.)


###Frontend

1) Instalar o NodeJS.

2) Ap�s instalado, executar os seguintes comandos via terminal: 

  npm install -g bower
  npm install -g grunt

3) Entre na pasta frontend do prjeto e execute no terminal:
  npm install
  bower install

4) Para executar o frontend rode:
  grunt serve

OBS: Assegure-se que as variaveis de ambiente 'java,npm e activator' foram adiocionadas corretamente ao PATH do sistema.


OBS2: Para abrir o projeto corretamente no IntelliJ � preciso instalar todos plugins da linguagem Scala.