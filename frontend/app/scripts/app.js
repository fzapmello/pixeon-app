(function() {
  'use strict';

  var app = angular.module('pixeonApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'ngGrid',
    'ui.bootstrap',
    'angular-loading-bar'
  ]);

  function run() {


  }

  app.run(run);
})();
