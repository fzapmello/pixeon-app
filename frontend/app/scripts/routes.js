(function() {
  'use strict';

  angular.module('pixeonApp').config(function($routeProvider) {


    $routeProvider

      .when('/login', {
        templateUrl: 'views/login/login.html',
        controller: 'LoginCtrl'
      })
      .when('/main', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/login'
      });
  });
})();
